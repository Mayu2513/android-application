package com.example.inficube.Activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.inficube.Adapter.SiteAllocationAdapter;
import com.example.inficube.Model.TSiteAllocation;
import com.example.inficube.R;
import com.example.inficube.View.CustomProgress;
import com.example.inficube.View.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SiteAllocationActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView titleText;
    private LinearLayout backLayoutclick;
    private RecyclerView recyclerView;
    SiteAllocationAdapter adapter;
    SharedPreferences mSharedPreferences;
    RequestQueue requestQueue;
    CustomProgress customProgress = CustomProgress.getInstance();
    ArrayList<TSiteAllocation> tSiteAllocationArrayList = new ArrayList<>();

    String itemID = "";
    String ID = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_site_allocation);
        requestQueue = Volley.newRequestQueue(this);


        Intent intent = getIntent();
        itemID = intent.getStringExtra("itemID");

        init();
    }

    private void init() {
        titleText = findViewById(R.id.titleText);
        recyclerView = findViewById(R.id.recyclerView);
        backLayoutclick = findViewById(R.id.backLayoutclick);

        titleText.setText("My Sites");

        backLayoutclick.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.backLayoutclick:
                super.onBackPressed();
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mSharedPreferences = getSharedPreferences(Utils.PREF_NAME, Context.MODE_PRIVATE);
        ID = mSharedPreferences.getString(Utils.ID, "");
        customProgress.showProgress(this, false);
        getAllocationSites();
    }

    private void getAllocationSites() {
        Map<String, String> params = new HashMap<String, String>();
        params.put("id", ID);
        JSONObject jsonObj = new JSONObject(params);

        Log.e("PARA : ->", jsonObj.toString());
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, Utils.ALLOCATION_SITES_URL, jsonObj,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.e("Site List : ->", response.toString());
                            int status = response.getInt("status");
                            if (status == 1) {
                                if (response.has("data") && !response.isNull("data")) {

                                    JSONArray dataArray = response.getJSONArray("data");
                                    if (dataArray.length() > 0) {
                                        setSiteData(dataArray);
                                    } else {
                                        Toast.makeText(SiteAllocationActivity.this, response.getString("message"), Toast.LENGTH_SHORT).show();
                                    }
                                } else {
                                    Toast.makeText(SiteAllocationActivity.this, response.getString("message"), Toast.LENGTH_SHORT).show();

                                }


                            } else {
                                Toast.makeText(SiteAllocationActivity.this, response.getString("message"), Toast.LENGTH_SHORT).show();
                            }

                            customProgress.hideProgress();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e("Error: ", error.getMessage());
                customProgress.hideProgress();
            }
        });
        requestQueue.add(jsonObjReq);
    }


    private void setSiteData(JSONArray jsonArray) {
        if (jsonArray.length() > 0) {
            for (int i = 0; i < jsonArray.length(); i++) {
                try {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    TSiteAllocation tSiteAllocation = new TSiteAllocation();
                    tSiteAllocation.setSiteName(jsonObject.getString("siteName"));
                    tSiteAllocation.setSiteDate(Utils.getDate(jsonObject.getString("jobDateTime")));
                    tSiteAllocation.setSiteTime(Utils.getTime(jsonObject.getString("jobDateTime")));

                    tSiteAllocationArrayList.add(tSiteAllocation);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            LinearLayoutManager gridLayoutManager = new LinearLayoutManager(getApplicationContext());
            gridLayoutManager.setOrientation(gridLayoutManager.VERTICAL);
            recyclerView.setLayoutManager(gridLayoutManager);
            adapter = new SiteAllocationAdapter(this, tSiteAllocationArrayList, itemClick);
            recyclerView.setAdapter(adapter);
        } else {
            Toast.makeText(SiteAllocationActivity.this, "No Record Found", Toast.LENGTH_SHORT).show();

        }
    }

    View.OnClickListener itemClick = v -> {
        int index = (int) v.getTag();
        Log.d("INDEX", "" + index);
        showAlertDialog();


//        Intent i =new Intent(this, SiteDetailsActivity.class);
//        startActivity(i);
    };

    public void showAlertDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final View customLayout = getLayoutInflater().inflate(R.layout.alert_dialog_inprocess, null);
        Button okBtn = customLayout.findViewById(R.id.okBtn);

        builder.setView(customLayout);
        final AlertDialog dialog = builder.create();

        okBtn.setOnClickListener(v -> dialog.hide());
        dialog.show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}