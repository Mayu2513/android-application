package com.example.inficube.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;

import androidx.recyclerview.widget.RecyclerView;

import com.example.inficube.Model.SiteDetails.TSiteDetailAllocation;
import com.example.inficube.Model.TSiteAllocation;
import com.example.inficube.R;

import java.util.ArrayList;

public class SiteServiceAllocationAdapter extends RecyclerView.Adapter<SiteServiceAllocationAdapter.ViewHolder> {

    ArrayList<TSiteDetailAllocation> tSiteAllocations;
    Activity activity;

    public SiteServiceAllocationAdapter(Context context, ArrayList<TSiteDetailAllocation> tSiteAllocations) {
        super();
        this.activity = (Activity) context;
        this.tSiteAllocations = tSiteAllocations;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View v = layoutInflater.inflate(R.layout.activity_site_service_item_view, parent, false);
        RecyclerView.ViewHolder holder = new ViewHolder(v);
        return (ViewHolder) holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        TSiteDetailAllocation tSiteAllocation = tSiteAllocations.get(position);

        holder.serviceCheckBox.setText("" + tSiteAllocation.getServiceName());

    }

    @Override
    public int getItemCount() {
        return tSiteAllocations.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private CheckBox serviceCheckBox;
        private LinearLayout itemLayout;

        public ViewHolder(View view) {
            super(view);
            serviceCheckBox = view.findViewById(R.id.serviceCheckBox);
            itemLayout = view.findViewById(R.id.itemLayout);
        }
    }
}


