package com.example.inficube.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.inficube.Model.TSiteAllocation;
import com.example.inficube.R;

import java.util.ArrayList;

public class SiteAllocationAdapter extends RecyclerView.Adapter<SiteAllocationAdapter.ViewHolder> {

    ArrayList<TSiteAllocation> tSiteAllocations;
    Activity activity;
    View.OnClickListener onClickListener;

    public SiteAllocationAdapter(Context context, ArrayList<TSiteAllocation> tSiteAllocations, View.OnClickListener onClickListener) {
        super();
        this.activity = (Activity) context;
        this.tSiteAllocations = tSiteAllocations;
        this.onClickListener = onClickListener;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View v = layoutInflater.inflate(R.layout.activity_site_allocation_item_view, parent, false);
        RecyclerView.ViewHolder holder = new ViewHolder(v);
        return (ViewHolder) holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        TSiteAllocation tSiteAllocation = tSiteAllocations.get(position);

        holder.siteNameView.setText("Site : " + tSiteAllocation.getSiteName());
        holder.siteDateView.setText("Date : " + tSiteAllocation.getSiteDate());
        holder.siteTimeView.setText("Time : " + tSiteAllocation.getSiteTime());

        holder.itemLayout.setTag(position);
        holder.itemLayout.setOnClickListener(onClickListener);
    }

    @Override
    public int getItemCount() {
        return tSiteAllocations.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView siteNameView;
        private TextView siteTimeView;
        private TextView siteDateView;
        private LinearLayout itemLayout;

        public ViewHolder(View view) {
            super(view);
            siteNameView = view.findViewById(R.id.siteNameView);
            siteDateView = view.findViewById(R.id.siteDateView);
            siteTimeView = view.findViewById(R.id.siteTimeView);
            itemLayout = view.findViewById(R.id.itemLayout);
        }
    }
}


