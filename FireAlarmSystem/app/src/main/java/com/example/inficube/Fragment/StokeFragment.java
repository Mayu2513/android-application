package com.example.inficube.Fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.inficube.Activity.MainActivity;
import com.example.inficube.Adapter.ProductAllocationAdapter;
import com.example.inficube.Model.TProductAllocation;
import com.example.inficube.R;
import com.example.inficube.View.CustomProgress;
import com.example.inficube.View.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class StokeFragment extends Fragment {

    private View parentLayout;
    private RecyclerView recyclerView;
    ProductAllocationAdapter adapter;
    SharedPreferences mSharedPreferences;
    RequestQueue requestQueue;
    CustomProgress customProgress = CustomProgress.getInstance();
    ArrayList<TProductAllocation> tProductAllocationsArrayList = new ArrayList<>();

    String itemID = "";
    String ID = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        parentLayout = inflater.inflate(R.layout.stoke_fragment_view, container, false);
        requestQueue = Volley.newRequestQueue(getActivity());
        mSharedPreferences = getActivity().getSharedPreferences(Utils.PREF_NAME, Context.MODE_PRIVATE);
        ID = mSharedPreferences.getString(Utils.ID, "");

        init();

        return parentLayout;
    }

    private void init() {
        ((MainActivity) getActivity()).setTitle("My Stock", false);
        recyclerView = parentLayout.findViewById(R.id.recyclerView);
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).setTitle("My Stock", false);
        customProgress.showProgress(getActivity(), false);
        getAllocationProducts();
    }

    private void getAllocationProducts() {
        Map<String, String> params = new HashMap<String, String>();
        params.put("id", ID);
        JSONObject jsonObj = new JSONObject(params);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, Utils.ALLOCATION_PRODUCT_URL, jsonObj,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("Product ::", String.valueOf(response));
                        try {
                            int status = response.getInt("status");
                            if (status == 1) {
                                JSONArray dataArray = response.getJSONArray("data");
                                if (response.has("data") && !response.isNull("data")) {
                                    setProductData(dataArray);
                                } else {
                                    Toast.makeText(getActivity(), response.getString("message"), Toast.LENGTH_SHORT).show();
                                }

                            } else {
                                Toast.makeText(getActivity(), response.getString("message"), Toast.LENGTH_SHORT).show();
                            }

                            customProgress.hideProgress();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e("Error: ", error.getMessage());
                customProgress.hideProgress();
            }
        });
        requestQueue.add(jsonObjReq);

    }

    private void setProductData(JSONArray jsonArray) {

        if (jsonArray.length() > 0) {

            for (int i = 0; i < jsonArray.length(); i++) {
                try {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    TProductAllocation tProductAllocation = new TProductAllocation();
                    tProductAllocation.setProductID(jsonObject.getString("productId"));
                    tProductAllocation.setProductName(jsonObject.getString("productName"));
                    tProductAllocation.setQuantity(jsonObject.getString("quantity"));

                    tProductAllocationsArrayList.add(tProductAllocation);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            LinearLayoutManager gridLayoutManager = new LinearLayoutManager(getActivity());
            gridLayoutManager.setOrientation(gridLayoutManager.VERTICAL);
            recyclerView.setLayoutManager(gridLayoutManager);
            adapter = new ProductAllocationAdapter(getActivity(), tProductAllocationsArrayList);
            recyclerView.setAdapter(adapter);
        } else {
            Toast.makeText(getActivity(), "No Record Found", Toast.LENGTH_SHORT).show();

        }


    }
}
