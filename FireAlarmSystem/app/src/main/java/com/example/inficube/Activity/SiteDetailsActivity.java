package com.example.inficube.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.inficube.Adapter.ProductAllocationAdapter;
import com.example.inficube.Adapter.SiteServiceAllocationAdapter;
import com.example.inficube.Model.SiteDetails.TSiteDetailAllocation;
import com.example.inficube.Model.TProductAllocation;
import com.example.inficube.R;
import com.example.inficube.View.CustomProgress;
import com.example.inficube.View.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.example.inficube.View.CustomProgress.customProgress;

public class SiteDetailsActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView titleText;
    private LinearLayout backLayoutclick;
    private RecyclerView recyclerView;
    RequestQueue requestQueue;
    CustomProgress customProgress = CustomProgress.getInstance();
    String ID = "";
    SiteServiceAllocationAdapter siteServiceAllocationAdapter;
    ArrayList<TSiteDetailAllocation> tSiteDetailAllocations = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_site_details);
        requestQueue = Volley.newRequestQueue(this);

        init();
    }

    private void init() {
        titleText = findViewById(R.id.titleText);
        recyclerView = findViewById(R.id.recyclerView);
        backLayoutclick = findViewById(R.id.backLayoutclick);

        titleText.setText("Site Details");

        backLayoutclick.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getSiteDetails();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.backLayoutclick:
                super.onBackPressed();
                break;
        }
    }

    private void getSiteDetails() {

        Map<String, String> params = new HashMap<String, String>();
        params.put("id", ID);
        JSONObject jsonObj = new JSONObject(params);

        Log.e("PARA : ->", jsonObj.toString());
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, Utils.SITE_DETAILS_URL, jsonObj,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.e("Site details : ->", response.toString());
                            int status = response.getInt("status");
                            if (status == 1) {

                                if (response.has("data") && !response.isNull("data")) {
                                    JSONObject dataObj = response.getJSONObject("data");
                                    JSONArray serviceArray = dataObj.getJSONArray("services");
                                    if(serviceArray.length() > 0){
                                        setServices(serviceArray);
                                    }
                                }

                            } else {
                                Toast.makeText(SiteDetailsActivity.this, response.getString("message"), Toast.LENGTH_SHORT).show();

                            }

                            customProgress.hideProgress();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e("Error: ", error.getMessage());
                customProgress.hideProgress();
            }
        });
        requestQueue.add(jsonObjReq);


    }

    private void setServices(JSONArray jsonArray){
        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                TSiteDetailAllocation tSiteDetailAllocation = new TSiteDetailAllocation();
                tSiteDetailAllocation.setServiceID(jsonObject.getString("id"));
                tSiteDetailAllocation.setServiceName(jsonObject.getString("name"));

                tSiteDetailAllocations.add(tSiteDetailAllocation);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        LinearLayoutManager gridLayoutManager = new LinearLayoutManager(this);
        gridLayoutManager.setOrientation(gridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(gridLayoutManager);
        siteServiceAllocationAdapter = new SiteServiceAllocationAdapter(getApplicationContext(), tSiteDetailAllocations);
        recyclerView.setAdapter(siteServiceAllocationAdapter);
    }

}