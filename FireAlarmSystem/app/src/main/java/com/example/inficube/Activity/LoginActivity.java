package com.example.inficube.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.inficube.R;
import com.example.inficube.View.CustomProgress;
import com.example.inficube.View.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "LoginActivity";
    CustomProgress customProgress = CustomProgress.getInstance();
    RequestQueue requestQueue;
    SharedPreferences mSharedPreferences;
    private EditText eEmail;
    private EditText ePassword;
    private Button btnSignIn;

    private String email;
    private String password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        requestQueue = Volley.newRequestQueue(this);
        mSharedPreferences = getSharedPreferences(Utils.PREF_NAME, Context.MODE_PRIVATE);
        init();
    }

    private void init() {
        eEmail = findViewById(R.id.eEmail);
        ePassword = findViewById(R.id.ePassword);
        btnSignIn = findViewById(R.id.btnSignIn);

        btnSignIn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSignIn:
                dologin();
                break;
        }
    }

    public void dologin() {
        Log.d(TAG, "Login");

        if (!validate()) {
            return;
        }

        customProgress.showProgress(this, true);
        login();
    }

    public boolean validate() {
        boolean valid = true;

        email = eEmail.getText().toString();
        password = ePassword.getText().toString();

       if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
           Toast.makeText(this, R.string.enter_email, Toast.LENGTH_SHORT).show();
           valid = false;
       }

       if (password.isEmpty()) {
           Toast.makeText(this, R.string.enter_password, Toast.LENGTH_SHORT).show();
           valid = false;
       }

        return valid;
    }

    private void login() {
        Map<String, String> params = new HashMap<String, String>();

        params.put("UserName", email);
        params.put("Password", password);
        JSONObject jsonObj = new JSONObject(params);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, Utils.LOGIN_URL, jsonObj,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            int status = response.getInt("status");
                            if (status == 1) {
                                Toast.makeText(LoginActivity.this, "Login Successfully...", Toast.LENGTH_SHORT).show();
                                loginData(response);
                            } else {
                                Toast.makeText(LoginActivity.this, response.getString("message"), Toast.LENGTH_SHORT).show();
                            }

                            customProgress.hideProgress();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e("Error: ", error.getMessage());
                customProgress.hideProgress();
            }
        });
        requestQueue.add(jsonObjReq);

    }

    private void loginData(JSONObject response) throws JSONException {

        JSONObject dataObj = response.getJSONObject("data");
        if (dataObj != null) {
            Utils.storeString(mSharedPreferences, Utils.LOGIN_DATA, dataObj.toString());
            Utils.storeString(mSharedPreferences, Utils.ID, dataObj.getString("id"));

            Intent i = new Intent(this, MainActivity.class);
            startActivity(i);
            finish();
        } else {
            Toast.makeText(LoginActivity.this, "Data is empty...", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}