package com.example.inficube.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.inficube.Model.TProductAllocation;
import com.example.inficube.R;

import java.util.ArrayList;

public class ProductAllocationAdapter extends RecyclerView.Adapter<ProductAllocationAdapter.ViewHolder> {

    ArrayList<TProductAllocation> tProductAllocations;
    Activity activity;

    public ProductAllocationAdapter(Context context, ArrayList<TProductAllocation> tProductAllocations) {
        super();
        this.activity = (Activity) context;
        this.tProductAllocations = tProductAllocations;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View v = layoutInflater.inflate(R.layout.activity_product_allocation_item_view, parent, false);
        RecyclerView.ViewHolder holder = new ViewHolder(v);
        return (ViewHolder) holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        TProductAllocation tProductAllocation = tProductAllocations.get(position);

        holder.productNameView.setText("Name : " + tProductAllocation.getProductName());
        holder.quantityProductView.setText("Quantity : " + tProductAllocation.getQuantity());

//        holder.itemLayout.setTag(position);
//        holder.itemLayout.setOnClickListener(onClickListener);
    }

    @Override
    public int getItemCount() {
        return tProductAllocations.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView productNameView;
        private TextView quantityProductView;
        private LinearLayout itemLayout;

        public ViewHolder(View view) {
            super(view);
            productNameView = view.findViewById(R.id.productNameView);
            quantityProductView = view.findViewById(R.id.quantityProductView);
            itemLayout = view.findViewById(R.id.itemLayout);
        }
    }
}


