package com.example.inficube.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.example.inficube.Activity.MainActivity;
import com.example.inficube.Activity.SiteAllocationActivity;
import com.example.inficube.R;
import com.example.inficube.View.Utils;

public class SiteFragment extends Fragment implements View.OnClickListener {

    private View parentLayout;
    private TextView todayView;
    private TextView thisWeekView;
    private TextView thisMonthView;
    private TextView allView;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        parentLayout = inflater.inflate(R.layout.site_fragment_view, container, false);
        init();

        return parentLayout;
    }

    public void init() {
        ((MainActivity) getActivity()).setTitle("My Jobs", false);
        todayView = parentLayout.findViewById(R.id.todayView);
        thisWeekView = parentLayout.findViewById(R.id.thisWeekView);
        thisMonthView = parentLayout.findViewById(R.id.thisMonthView);
        allView = parentLayout.findViewById(R.id.allView);

        todayView.setOnClickListener(this);
        thisWeekView.setOnClickListener(this);
        thisMonthView.setOnClickListener(this);
        allView.setOnClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).setTitle("My Jobs", false);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.todayView:
                showAlertDialog();
                break;
            case R.id.thisWeekView:
                showAlertDialog();
                break;
            case R.id.thisMonthView:
                showAlertDialog();
                break;
            case R.id.allView:
                Intent i4 = new Intent(getActivity(), SiteAllocationActivity.class);
                i4.putExtra("itemID", Utils.ALL);
                startActivity(i4);
                break;
        }
    }

    public void showAlertDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final View customLayout = getLayoutInflater().inflate(R.layout.alert_dialog_inprocess, null);
        Button okBtn = customLayout.findViewById(R.id.okBtn);

        builder.setView(customLayout);
        final AlertDialog dialog = builder.create();

        okBtn.setOnClickListener(v -> dialog.hide());
        dialog.show();
    }


}
