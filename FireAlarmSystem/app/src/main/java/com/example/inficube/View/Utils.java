package com.example.inficube.View;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class Utils {

    public static String PREF_NAME = "FIRE_SYSTEM";
    public static String SERVER_URL = "http://inficubesolutions.com/API/";

    public static String LOGIN_URL = SERVER_URL + "Login/LoginCheck";
    public static String ALLOCATION_SITES_URL = SERVER_URL + "SiteAllocation/GetAllSiteAllocation";
    public static String ALLOCATION_PRODUCT_URL = SERVER_URL + "ProductAllocations/GetAllocatedProducts";
    public static String SITE_DETAILS_URL = SERVER_URL + "SiteAllocation/GetAllSiteAllocationDetailsById";

    public static int pageClick = 0;
    public static String TODAY = "TODAY";
    public static String THIS_WEEK = "THIS_WEEK";
    public static String THIS_MONTH = "THIS_MONTH";
    public static String ALL = "ALL";

    /*---------preference string-------------*/
    public static String LOGIN_DATA = "LOGIN_DATA";
    public static String ID = "ID";

    public static void storeString(SharedPreferences sharedPreferences, String key, String value) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static boolean checkInternetConnection(Context context) {
        ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }

//    public static Typeface getLightTypeFace(Context context) {
//        Typeface typeface = Typeface.createFromAsset(context.getAssets(), "SourceSansProLight.ttf");
//        return typeface;
//    }
//
//    public static Typeface getRegularTypeFace(Context context) {
//        Typeface typeface = Typeface.createFromAsset(context.getAssets(), "SourceSansProRegular.ttf");
//        return typeface;
//    }
//
//    public static Typeface getsemiBoldTypeFace(Context context) {
//        Typeface typeface = Typeface.createFromAsset(context.getAssets(), "SourceSansPro-Semibold.ttf");
//        return typeface;
//    }

    public static String getDate(String timeFormate) {

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");
        Date result = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat();
        try {
            result = df.parse(timeFormate);
            System.out.println("date:" + result); //prints date in current locale
            sdf = new SimpleDateFormat("dd-MM-yyyy");
            sdf.setTimeZone(TimeZone.getTimeZone("GMT+1"));
            System.out.println(sdf.format(result));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return sdf.format(result);
    }


    public static String getTime(String timeFormate) {

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");
        Date result = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat();
        try {
            result = df.parse(timeFormate);
            System.out.println("time:" + result); //prints date in current locale
            sdf = new SimpleDateFormat("HH:mm:ss");
            sdf.setTimeZone(TimeZone.getTimeZone("GMT+1"));
            System.out.println(sdf.format(result));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return sdf.format(result);
    }


}
