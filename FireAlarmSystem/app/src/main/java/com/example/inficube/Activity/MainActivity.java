package com.example.inficube.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import com.example.inficube.Fragment.HomeFragment;
import com.example.inficube.Fragment.ProfileFragment;
import com.example.inficube.Fragment.SiteFragment;
import com.example.inficube.Fragment.StokeFragment;
import com.example.inficube.R;
import com.example.inficube.View.Utils;
import com.google.android.material.navigation.NavigationView;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    SharedPreferences mSharedPreferences;
    Toolbar mToolbar;
    FrameLayout FrameLayout;
    DrawerLayout mDrawerLayout;
    NavigationView mNavigationView;

    private ImageView userImage;
    private TextView userNameTV;
    private LinearLayout logoutLayout;
    private LinearLayout homeLayout;
    private LinearLayout jobLayout;
    private LinearLayout stokeLayout;
    private FrameLayout profileLayout;

    Fragment siteFragment;
    Fragment stokeFragment;
    Fragment homeFragment;
    Fragment profileFragment;

    private String userName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mSharedPreferences = getSharedPreferences(Utils.PREF_NAME, Context.MODE_PRIVATE);
        init();

        String Logindata = mSharedPreferences.getString(Utils.LOGIN_DATA, "");

        if (!Logindata.equals("")) {
            try {
                JSONObject loginObj = new JSONObject(Logindata);
                userName = loginObj.getString("firstName") + " " + loginObj.getString("lastName");
                userNameTV.setText(userName);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        homeFragment = new HomeFragment();
        replaceFragment(homeFragment);


    }

    private void init() {
        FrameLayout = findViewById(R.id.FrameLayout);
        mNavigationView = findViewById(R.id.mNavigationView);

        setUpToolbar();
        setupDrawerContent();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpToolbar();
        setupDrawerContent();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.logoutLayout:
                showLogoutAlertDialog();
                break;
            case R.id.profileLayout:
                profileFragment = new ProfileFragment();
                replaceFragment(profileFragment);
                Utils.pageClick = 1;
                break;
            case R.id.homeLayout:
                homeFragment = new HomeFragment();
                replaceFragment(homeFragment);
                Utils.pageClick = 0;
                break;
            case R.id.jobLayout:
                siteFragment = new SiteFragment();
                replaceFragment(siteFragment);
                Utils.pageClick = 1;
                break;
            case R.id.stokeLayout:
                stokeFragment = new StokeFragment();
                replaceFragment(stokeFragment);
                Utils.pageClick = 1;
                break;
        }

        mDrawerLayout.closeDrawer(GravityCompat.START);
    }


    @Override
    public void onBackPressed() {
        if (Utils.pageClick == 0) {
            super.onBackPressed();
        } else {
            homeFragment = new HomeFragment();
            replaceFragment(homeFragment);
            Utils.pageClick = 0;
        }
    }

    private void setupDrawerContent() {
        userNameTV = mNavigationView.findViewById(R.id.userNameTV);
        userImage = mNavigationView.findViewById(R.id.userImage);
        logoutLayout = mNavigationView.findViewById(R.id.logoutLayout);
        homeLayout = mDrawerLayout.findViewById(R.id.homeLayout);
        jobLayout = mNavigationView.findViewById(R.id.jobLayout);
        stokeLayout = mNavigationView.findViewById(R.id.stokeLayout);
        profileLayout = mNavigationView.findViewById(R.id.profileLayout);

        logoutLayout.setOnClickListener(this);
        homeLayout.setOnClickListener(this);
        jobLayout.setOnClickListener(this);
        stokeLayout.setOnClickListener(this);
        profileLayout.setOnClickListener(this);
    }

    TextView titleText;

    private void setUpToolbar() {
        mToolbar = findViewById(R.id.toolbar);

        mDrawerLayout = findViewById(R.id.nav_drawer);

        mToolbar.setNavigationIcon(R.drawable.menu_icon);
        if (mToolbar != null) {
            setSupportActionBar(mToolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            setTitle("Protect Fire Pvt Ltd", true);

            mToolbar.setNavigationOnClickListener(v -> mDrawerLayout.openDrawer(GravityCompat.START));
        }
    }

    public void setTitle(String title, boolean isBack) {
        getSupportActionBar().setHomeButtonEnabled(isBack);
        titleText = findViewById(R.id.titleText);
        titleText.setText(title);
    }

    public void replaceFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction().replace(R.id.FrameLayout, fragment).commit();
    }

    public void showLogoutAlertDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final View customLayout = getLayoutInflater().inflate(R.layout.alert_dialog, null);
        Button yesBtn = customLayout.findViewById(R.id.yesBtn);
        Button noBtn = customLayout.findViewById(R.id.noBtn);
        builder.setView(customLayout);
        final AlertDialog dialog = builder.create();
        yesBtn.setOnClickListener(v -> logout());

        noBtn.setOnClickListener(v -> dialog.hide());
        dialog.show();
    }

    private void logout() {
        Utils.storeString(mSharedPreferences, Utils.LOGIN_DATA, "");
        Utils.storeString(mSharedPreferences, Utils.ID, "");

        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(intent);
        finish();
    }

}