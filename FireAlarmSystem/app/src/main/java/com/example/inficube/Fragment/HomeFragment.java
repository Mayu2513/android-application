package com.example.inficube.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.example.inficube.Activity.MainActivity;
import com.example.inficube.R;
import com.example.inficube.View.Utils;

public class HomeFragment extends Fragment implements View.OnClickListener {

    private View parentLayout;
    private TextView gotoJobView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        parentLayout = inflater.inflate(R.layout.fragment_home, container, false);
        init();

        return parentLayout;
    }

    public void init() {
        ((MainActivity) getActivity()).setTitle("Protect Fire Pvt Ltd");
        gotoJobView = parentLayout.findViewById(R.id.gotoJobView);

        gotoJobView.setOnClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).setTitle("Protect Fire Pvt Ltd", true);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.gotoJobView:
                SiteFragment siteFragment = new SiteFragment();
                replaceFragment(siteFragment);
                Utils.pageClick = 1;
                break;
        }
    }

    public void replaceFragment(Fragment fragment) {
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.FrameLayout, fragment).commit();
    }
}
