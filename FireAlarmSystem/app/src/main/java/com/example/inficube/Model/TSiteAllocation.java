package com.example.inficube.Model;

public class TSiteAllocation {

    String siteName, siteDate, siteTime;

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public String getSiteDate() {
        return siteDate;
    }

    public void setSiteDate(String siteDate) {
        this.siteDate = siteDate;
    }

    public String getSiteTime() {
        return siteTime;
    }

    public void setSiteTime(String siteTime) {
        this.siteTime = siteTime;
    }
}
