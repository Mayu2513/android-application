package com.example.inficube.Activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Toast;

import com.example.inficube.R;
import com.example.inficube.View.Utils;

public class SplashscreenActivity extends Activity {
    private static int SPLASH_TIME_OUT = 2000;
    SharedPreferences settings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.splash_screen);

        settings = getApplicationContext().getSharedPreferences(Utils.PREF_NAME, 0);

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

                if (Utils.checkInternetConnection(SplashscreenActivity.this)) {
                    if (!settings.getString(Utils.ID, "").isEmpty()) {
                        Intent i = new Intent(SplashscreenActivity.this, MainActivity.class);
                        startActivity(i);
                        finish();

                    } else {
                        Intent i = new Intent(SplashscreenActivity.this, LoginActivity.class);
                        startActivity(i);
                        finish();
                    }
                } else {
                    Toast.makeText(SplashscreenActivity.this, "Please check your internet connection!", Toast.LENGTH_SHORT).show();
                }
            }
        }, SPLASH_TIME_OUT);
    }
}
