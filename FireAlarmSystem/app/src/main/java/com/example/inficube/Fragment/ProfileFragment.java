package com.example.inficube.Fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.example.inficube.Activity.MainActivity;
import com.example.inficube.R;
import com.example.inficube.View.Utils;

import org.json.JSONException;
import org.json.JSONObject;

public class ProfileFragment extends Fragment {

    private View parentLayout;
    SharedPreferences mSharedPreferences;

    private TextView emailTV;
    private TextView mobileTV;
    private TextView dobTV;
    private TextView addressTV;
    private TextView userNameTV;

    private String userName;
    private String email;
    private String mobile;
    private String dob;
    private String address;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        parentLayout = inflater.inflate(R.layout.profile_fragment_view, container, false);
        mSharedPreferences = getActivity().getSharedPreferences(Utils.PREF_NAME, Context.MODE_PRIVATE);
        init();

        return parentLayout;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).setTitle("My Profile", false);
    }

    public void init() {

        userNameTV = parentLayout.findViewById(R.id.userNameTV);
        emailTV = parentLayout.findViewById(R.id.emailTV);
        mobileTV = parentLayout.findViewById(R.id.mobileTV);
        dobTV = parentLayout.findViewById(R.id.dobTV);
        addressTV = parentLayout.findViewById(R.id.addressTV);

        String Logindata = mSharedPreferences.getString(Utils.LOGIN_DATA, "");

        if (!Logindata.equals("")) {
            try {
                JSONObject loginObj = new JSONObject(Logindata);
                userName = loginObj.getString("firstName") + " " + loginObj.getString("lastName");
                email = loginObj.getString("email");
                mobile = loginObj.getString("phone");
                dob = Utils.getDate(loginObj.getString("dateOfBirth"));
                address = loginObj.getString("address");

                userNameTV.setText(userName);
                emailTV.setText(email);
                mobileTV.setText(mobile);
                dobTV.setText(dob);
                addressTV.setText(address);


            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
